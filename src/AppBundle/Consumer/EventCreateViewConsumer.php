<?php

namespace AppBundle\Consumer;

use AppBundle\Service\DAO\EventViewService;
use Exception;
use OldSound\RabbitMqBundle\RabbitMq\ConsumerInterface;
use PhpAmqpLib\Message\AMQPMessage;
use Psr\Log\LoggerInterface;

class EventCreateViewConsumer implements ConsumerInterface
{
    private $daoEventViewService;
    private $logger;

    public function __construct(
        EventViewService $daoEventViewService,
        LoggerInterface $logger
    ) {
        $this->daoEventViewService = $daoEventViewService;
        $this->logger = $logger;
    }

    public function execute(AMQPMessage $message)
    {
        $body = json_decode($message->body, true);

        try {
            $this->daoEventViewService->inc(new \DateTime($body['timestamp']), $body['country']);

            echo sprintf('VIEW created for country:%s @ %s ...', $body['country'], date('Y-m-d H:i:s')) . PHP_EOL;
        } catch (Exception $e) {
            $this->logError($message, $e->getMessage());
        }
    }

    private function logError($message, $error)
    {
        $data = [
            'error' => $error,
            'class' => __CLASS__,
            'message' => $message
        ];

        $this->logger->error(json_encode($data));
    }
}