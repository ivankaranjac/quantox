<?php

namespace AppBundle\Consumer;

use AppBundle\Service\DAO\EventPlayService;
use Exception;
use OldSound\RabbitMqBundle\RabbitMq\ConsumerInterface;
use PhpAmqpLib\Message\AMQPMessage;
use Psr\Log\LoggerInterface;

class EventCreatePlayConsumer implements ConsumerInterface
{
    private $daoEventPlayService;
    private $logger;

    public function __construct(
        EventPlayService $daoEventPlayService,
        LoggerInterface $logger
    ) {
        $this->daoEventPlayService = $daoEventPlayService;
        $this->logger = $logger;
    }

    public function execute(AMQPMessage $message)
    {
        $body = json_decode($message->body, true);

        try {
            $this->daoEventPlayService->inc(new \DateTime($body['timestamp']), $body['country']);

            echo sprintf('PLAY created for country:%s @ %s ...', $body['country'], date('Y-m-d H:i:s')) . PHP_EOL;
        } catch (Exception $e) {
            $this->logError($message, $e->getMessage());
        }
    }

    private function logError($message, $error)
    {
        $data = [
            'error' => $error,
            'class' => __CLASS__,
            'message' => $message
        ];

        $this->logger->error(json_encode($data));
    }
}