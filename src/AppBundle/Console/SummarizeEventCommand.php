<?php

namespace AppBundle\Console;

use AppBundle\Service\DAO\EventClickService;
use AppBundle\Service\DAO\EventPlayService;
use AppBundle\Service\DAO\EventViewService;
use AppBundle\Service\DAO\EventSummarizeService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class SummarizeEventCommand extends Command
{
    private $daoEventSummarizeService;
    private $daoEventViewService;
    private $daoEventClickService;
    private $daoEventPlayService;

    public function __construct(
        EventSummarizeService $daoEventSummarizeService,
        EventViewService $daoEventViewService,
        EventClickService $daoEventClickService,
        EventPlayService $daoEventPlayService
    )
    {
        $this->daoEventSummarizeService = $daoEventSummarizeService;
        $this->daoEventViewService = $daoEventViewService;
        $this->daoEventClickService = $daoEventClickService;
        $this->daoEventPlayService = $daoEventPlayService;

        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->setName('app:summarize-event')
            ->setDescription('Summarizes events for all time.')
            ->setHelp('This command runs once a day and gets all events for previous week day and adds to the all time summary')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->summarizeViews();
        $this->summarizeClicks();
        $this->summarizePlays();
    }

    private function summarizeViews()
    {
        /** @var \AppBundle\Entity\EventViewLog[] $events */
        $events = $this->daoEventViewService->findAllFrom7DaysAgo();

        foreach ($events as $event) {
            $this->daoEventSummarizeService->sumView($event->getCountry(), $event->getHits());
        }
    }

    private function summarizeClicks()
    {
        /** @var \AppBundle\Entity\EventClickLog[] $events */
        $events = $this->daoEventClickService->findAllFrom7DaysAgo();

        foreach ($events as $event) {
            $this->daoEventSummarizeService->sumView($event->getCountry(), $event->getHits());
        }
    }

    private function summarizePlays()
    {
        /** @var \AppBundle\Entity\EventPlayLog[] $events */
        $events = $this->daoEventPlayService->findAllFrom7DaysAgo();

        foreach ($events as $event) {
            $this->daoEventSummarizeService->sumView($event->getCountry(), $event->getHits());
        }
    }
}