<?php

namespace AppBundle\Service;

use AppBundle\Service\DAO\EventClickService;
use AppBundle\Service\DAO\EventPlayService;
use AppBundle\Service\DAO\EventSummarizeService;
use AppBundle\Service\DAO\EventViewService;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

use AppBundle\Event\Event;

class EventService
{
    private $dispatcher;

    private $daoSummarizeService;
    private $daoEventViewService;
    private $daoEventClickService;
    private $daoEventPlayService;

    public function __construct(
        EventDispatcherInterface $dispatcher,
        EventSummarizeService $daoSummarizeService,
        EventViewService $daoEventViewService,
        EventClickService $daoEventClickService,
        EventPlayService $daoEventPlayService
    )
    {
        $this->dispatcher = $dispatcher;
        $this->daoSummarizeService = $daoSummarizeService;
        $this->daoEventViewService = $daoEventViewService;
        $this->daoEventClickService = $daoEventClickService;
        $this->daoEventPlayService = $daoEventPlayService;
    }


    public function create($event, $country)
    {
        switch ($event) {
            case 'view': $type = Event::VIEW_CREATED; break;
            case 'click': $type = Event::CLICK_CREATED; break;
            case 'play': $type = Event::PLAY_CREATED; break;
            default:
                throw new \LogicException('Unknown event "' . $event . '"');
        }

        $this->dispatcher->dispatch($type, new Event($event, $country));
    }

    public function findTop5Countries($format)
    {
        $result = array(
            'views' =>array(),
            'clicks' =>array(),
            'plays' =>array()
        );

        /** @var \AppBundle\Entity\EventSummary[] $top5View */
        $top5View = $this->daoSummarizeService->findTop5CountriesView();

        foreach($top5View as $item) {
            $views = $this->daoEventViewService->sumPast7DaysForCountry($item->getCountry());

            if (!isset($result['views'][$item->getCountry()])) {
                $result['views'][$item->getCountry()] = array();
            }
            $result['views'][$item->getCountry()]['all_time'] = $item->getView() + $views;
            $result['views'][$item->getCountry()]['past_week'] = $views ?: 0;
        }

        /** @var \AppBundle\Entity\EventSummary[] $top5Click */
        $top5Click = $this->daoSummarizeService->findTop5CountriesClick();

        foreach($top5Click as $item) {
            $clicks = $this->daoEventClickService->sumPast7DaysForCountry($item->getCountry());

            if (!isset($result['clicks'][$item->getCountry()])) {
                $result['clicks'][$item->getCountry()] = array();
            }
            $result['clicks'][$item->getCountry()]['all_time'] = $item->getClick() + $clicks;
            $result['clicks'][$item->getCountry()]['past_week'] = $clicks ?: 0;
        }

        /** @var \AppBundle\Entity\EventSummary[] $top5Play */
        $top5Play = $this->daoSummarizeService->findTop5CountriesClick();

        foreach($top5Play as $item) {
            $plays = $this->daoEventPlayService->sumPast7DaysForCountry($item->getCountry());

            if (!isset($result['plays'][$item->getCountry()])) {
                $result['plays'][$item->getCountry()] = array();
            }
            $result['plays'][$item->getCountry()]['all_time'] = $item->getPlay() + $plays;
            $result['plays'][$item->getCountry()]['past_week'] = $plays ?: 0;
        }

        return $format === 'json' ? $result : $this->formatCSV($result);
    }

    private function formatCSV($data)
    {
        $output = array();

        $output[] = array('VIEW');
        $output[] = array('country', 'all_time', 'past_week');
        foreach ($data['views'] as $country => $payload) {
            $output[] = array($country, $payload['all_time'], $payload['past_week']);
        }
        $output[] = array();

        $output[] = array('CLICK');
        $output[] = array('country', 'all_time', 'past_week');
        foreach ($data['clicks'] as $country => $payload) {
            $output[] = array($country, $payload['all_time'], $payload['past_week']);
        }
        $output[] = array();

        $output[] = array('PLAY');
        $output[] = array('country', 'all_time', 'past_week');
        foreach ($data['plays'] as $country => $payload) {
            $output[] = array($country, $payload['all_time'], $payload['past_week']);
        }
        $output[] = array();

        return $output;
    }
}