<?php

namespace AppBundle\Service\DAO;

use AppBundle\Entity\EventClickLog;

use Doctrine\ORM\EntityManagerInterface;

class EventPlayService
{
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * Executes the insert ... on duplicate key update
     * @param  \DateTime $date
     * @param  string    $country
     * @return boolean           True if successful, false otherwise
     */
    public function inc(\DateTime $date, $country)
    {
        /** @var \AppBundle\Repository\EventViewRepository $repository */
        $repository = $this->entityManager->getRepository("AppBundle:EventPlayLog");

        // Try to increment
        if (0 < $repository->doInc($date, $country)) {
            // We were successful
            return true;
        }

        // Failed to increment, try to create new one
        $log = new EventClickLog($country, $date);

        try {
            $this->entityManager->persist($log);
            // Flush to generate \Exception if failure !
            $this->entityManager->flush();

            return true;
        } catch (\Doctrine\DBAL\DBALException $e) {
            // _One reason_ might be we have duplicate key
            return (bool) $repository->doInc($date, $country);
        }
    }

    public function findAllFrom7DaysAgo()
    {
        $weekAgo = new \DateTime('today - 7 days');

        return $this->entityManager->getRepository('AppBundle:EventPlayLog')->findAllFrom7DaysAgo($weekAgo);
    }

    public function sumPast7DaysForCountry($country)
    {
        $weekAgo = new \DateTime('today - 7 days');

        $result = $this->entityManager->getRepository('AppBundle:EventPlayLog')->sumPast7DaysForCountry($country, $weekAgo);

        return $result;
    }
}