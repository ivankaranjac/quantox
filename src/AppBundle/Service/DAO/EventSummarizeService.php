<?php

namespace AppBundle\Service\DAO;

use AppBundle\Entity\EventSummary;
use Doctrine\ORM\EntityManagerInterface;

class EventSummarizeService
{
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * Executes the insert ... on duplicate key update
     * @param  string    $country
     * @param  int       $hits
     * @return boolean           True if successful, false otherwise
     */
    public function sumView($country, $hits)
    {
        /** @var \AppBundle\Repository\EventSummaryRepository $repository */
        $repository = $this->entityManager->getRepository("AppBundle:EventSummary");

        // Try to increment
        if (0 < $repository->doSumView($country, $hits)) {
            // We were successful
            return true;
        }

        // Failed to increment, try to create new one
        $sum = new EventSummary($country);
        $sum->setView($hits);

        try {
            $this->entityManager->persist($sum);
            // Flush to generate \Exception if failure !
            $this->entityManager->flush();

            return true;
        } catch (\Doctrine\DBAL\DBALException $e) {
            // _One reason_ might be we have duplicate key
            return (bool) $repository->doSumView($country, $hits);
        }
    }

    /**
     * Executes the insert ... on duplicate key update
     * @param  string    $country
     * @param  int       $hits
     * @return boolean           True if successful, false otherwise
     */
    public function sumClick($country, $hits)
    {
        /** @var \AppBundle\Repository\EventSummaryRepository $repository */
        $repository = $this->entityManager->getRepository("AppBundle:EventSummary");

        // Try to increment
        if (0 < $repository->doSumClick($country, $hits)) {
            // We were successful
            return true;
        }

        // Failed to increment, try to create new one
        $sum = new EventSummary($country);
        $sum->setClick($hits);

        try {
            $this->entityManager->persist($sum);
            // Flush to generate \Exception if failure !
            $this->entityManager->flush();

            return true;
        } catch (\Doctrine\DBAL\DBALException $e) {
            // _One reason_ might be we have duplicate key
            return (bool) $repository->doSumClick($country, $hits);
        }
    }

    /**
     * Executes the insert ... on duplicate key update
     * @param  string    $country
     * @param  int       $hits
     * @return boolean           True if successful, false otherwise
     */
    public function sumPlay($country, $hits)
    {
        /** @var \AppBundle\Repository\EventSummaryRepository $repository */
        $repository = $this->entityManager->getRepository("AppBundle:EventSummary");

        // Try to increment
        if (0 < $repository->doSumPlay($country, $hits)) {
            // We were successful
            return true;
        }

        // Failed to increment, try to create new one
        $sum = new EventSummary($country);
        $sum->setPlay($hits);

        try {
            $this->entityManager->persist($sum);
            // Flush to generate \Exception if failure !
            $this->entityManager->flush();

            return true;
        } catch (\Doctrine\DBAL\DBALException $e) {
            // _One reason_ might be we have duplicate key
            return (bool) $repository->doSumPlay($country, $hits);
        }
    }

    public function findTop5CountriesView()
    {
        return $this->entityManager->getRepository("AppBundle:EventSummary")->findTop5CountriesView();
    }

    public function findTop5CountriesClick()
    {
        return $this->entityManager->getRepository("AppBundle:EventSummary")->findTop5CountriesClick();
    }

    public function findTop5CountriesPlay()
    {
        return $this->entityManager->getRepository("AppBundle:EventSummary")->findTop5CountriesPlay();
    }
}