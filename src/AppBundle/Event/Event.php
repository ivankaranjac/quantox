<?php

namespace AppBundle\Event;

use Symfony\Component\EventDispatcher\Event as BaseEvent;

class Event extends BaseEvent
{
    const VIEW_CREATED = 'events.view.created';
    const CLICK_CREATED = 'events.click.created';
    const PLAY_CREATED = 'events.play.created';

    private $event;
    private $country;

    public function __construct($event, $country)
    {
        $this->event = $event;
        $this->country = $country;
    }

    public function getEvent()
    {
        return $this->event;
    }

    public function getCountry()
    {
        return $this->country;
    }
}