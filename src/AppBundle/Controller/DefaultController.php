<?php

namespace AppBundle\Controller;

use AppBundle\Http\CsvResponse;
use AppBundle\Http\ErrorJsonResponse;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Component\Validator\Constraints as Assert;

use AppBundle\Service\EventService;

class DefaultController extends Controller
{
    /**
     * @Method("POST")
     * @Route("/")
     */
    public function newAction(Request $request, EventService $eventService, ValidatorInterface $validator)
    {
        $data = json_decode($request->request->get('data', null));
        if (!$data) {
            return new ErrorJsonResponse(
                'badly formatted payload',
                ErrorJsonResponse::BAD_REQUEST
            );
        }

        // simple sanitation
        $event = trim(strtolower($data->event));
        $country = trim(strtoupper($data->country));

        // simple validation
        if (!in_array($event, array('view', 'click', 'play'))) {
            return new ErrorJsonResponse(
                '"event" param must one of [view, click, play]',
                ErrorJsonResponse::VALIDATION_ERROR
            );
        }
        $countryConstraint = new Assert\Country();
        $errors = $validator->validate($country, $countryConstraint);
        if (0 !== count($errors)) {
            return new ErrorJsonResponse(
                $errors[0]->getMessage(),
                ErrorJsonResponse::VALIDATION_ERROR
            );
        }

        $eventService->create($event, $country);
 
        return new JsonResponse('', 204);
    }

    /**
     * @Method("GET")
     * @Route("/")
     */
    public function listAction(Request $request, EventService $eventService)
    {
        if (in_array('application/json', $request->getAcceptableContentTypes())) {
            $format = 'json';
        }  else if (in_array('text/csv', $request->getAcceptableContentTypes())) {
            $format = 'csv';
        } else {
            return new ErrorJsonResponse(
                'accepted values for Content-Type header are [\'application/json\', \'text/csv\']',
                ErrorJsonResponse::BAD_REQUEST
            );
        }

        $result = $eventService->findTop5Countries($format);

        if ($format === 'json') {
            return new JsonResponse($result, 200, ['Content-Type' =>'application/json']);
        } else {
            return new CsvResponse('report', $result);
        }

    }

}
