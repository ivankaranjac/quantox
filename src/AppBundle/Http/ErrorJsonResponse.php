<?php

namespace AppBundle\Http;

use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * @see https://tools.ietf.org/html/draft-nottingham-http-problem-07
 */
class ErrorJsonResponse extends JsonResponse
{
    const VALIDATION_ERROR = 'validation_error';
    const GENERAL_EXCEPTION = 'general_exception';
    const BAD_REQUEST = 'bad_request';

    public function __construct($msg = null, $status = self::GENERAL_EXCEPTION)
    {
        switch ($status) {
            case self::VALIDATION_ERROR: $code = 422; break;
            case self::BAD_REQUEST: $code = 400; break;
            default: $code = 500;
        }
        $data = array(
            'status' => $code,
            'type' => $status,
            'title' => $msg,
        );
        parent::__construct($data, $code, ['Content-Type' =>'application/problem+json'], false);
    }
}