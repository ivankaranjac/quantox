<?php

namespace AppBundle\Type;

class DateX extends \DateTime
{
    public function __toString()
    {
        return $this->format('U');
    }
}