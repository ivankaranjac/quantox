<?php

namespace AppBundle\Type;

use Doctrine\DBAL\Types\DateTimeType;
use Doctrine\DBAL\Platforms\AbstractPlatform;

class DateXType extends DateTimeType
{
    public function convertToPHPValue($value, AbstractPlatform $platform)
    {
        $dateTime = parent::convertToPHPValue($value, $platform);

        if ( ! $dateTime) {
            return $dateTime;
        }

        $val = new DateX('@' . $dateTime->format('U'));
        $val->setTimezone($dateTime->getTimezone());

        return $val;
    }

    public function getName()
    {
        return 'datex';
    }
}