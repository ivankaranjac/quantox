<?php

namespace AppBundle\EventListener;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use OldSound\RabbitMqBundle\RabbitMq\Producer;

use AppBundle\Event\Event;

class EventSubscriber implements EventSubscriberInterface
{
    private $producer;

    public function __construct(Producer $producer)
    {
        $this->producer = $producer;
    }

    public static function getSubscribedEvents()
    {
        // return the subscribed events, their methods and priorities
        return array(
            Event::VIEW_CREATED => array(
                array('processViewCreated')
            ),
            Event::CLICK_CREATED => array(
                array('processClickCreated')
            ),
            Event::PLAY_CREATED => array(
                array('processPlayCreated')
            )
        );
    }

    public function processViewCreated(Event $event)
    {
        $message = [
            'country' => $event->getCountry(),
            'timestamp' => date('Y-m-d H:i:s')
        ];

        $this->producer->publish(json_encode($message), 'view');
    }

    public function processClickCreated(Event $event)
    {
        $message = [
            'country' => $event->getCountry(),
            'timestamp' => date('Y-m-d H:i:s')
        ];

        $this->producer->publish(json_encode($message), 'click');
    }

    public function processPlayCreated(Event $event)
    {
        $message = [
            'country' => $event->getCountry(),
            'timestamp' => date('Y-m-d H:i:s')
        ];

        $this->producer->publish(json_encode($message), 'play');
    }
}