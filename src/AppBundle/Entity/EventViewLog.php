<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\EventViewRepository")
 * @ORM\Table(name="view_log")
 * @UniqueEntity(fields={"country", "date"})
 */
class EventViewLog
{
    /**
     * @var string
     * @ORM\Id()
     * @ORM\Column(type="string")
     */
    private $country;

    /**
     * @var \DateTime
     * @ORM\Id()
     * @ORM\Column(type="datex")
     */
    private $date;

    /**
     * @var int
     * @ORM\Column(type="integer")
     */
    private $hits;

    /**
     * EventViewLog constructor.
     * @param string $country
     * @param \DateTime $date
     */
    public function __construct($country, \DateTime $date)
    {
        $this->country = $country;
        $this->date = $date;
        $this->hits = 1;
    }


    /**
     * @return string
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * @return \DateTime
     */
    public function getDate()
    {
        return new \DateTime($this->date);
    }

    /**
     * @return int
     */
    public function getHits()
    {
        return $this->hits;
    }


}