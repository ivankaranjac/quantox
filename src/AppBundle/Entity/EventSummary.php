<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\EventSummaryRepository")
 * @ORM\Table(name="summary")
 */
class EventSummary
{
    /**
     * @var string
     * @ORM\Id()
     * @ORM\Column(type="string")
     */
    private $country;

    /**
     * @var int
     * @ORM\Column(type="integer", options={"default" : 0})
     */
    private $view;

    /**
     * @var int
     * @ORM\Column(type="integer", options={"default" : 0})
     */
    private $click;

    /**
     * @var int
     * @ORM\Column(type="integer", options={"default" : 0})
     */
    private $play;

    /**
     * @param string $country
     */
    public function __construct($country)
    {
        $this->country = $country;
        $this->view = 0;
        $this->click = 0;
        $this->play = 0;
    }


    /**
     * @return string
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * @return int
     */
    public function getView()
    {
        return $this->view;
    }

    /**
     * @param int $view
     */
    public function setView($view)
    {
        $this->view = $view;
    }

    /**
     * @return int
     */
    public function getClick()
    {
        return $this->click;
    }

    /**
     * @param int $click
     */
    public function setClick($click)
    {
        $this->click = $click;
    }

    /**
     * @return int
     */
    public function getPlay()
    {
        return $this->play;
    }

    /**
     * @param int $play
     */
    public function setPlay($play)
    {
        $this->play = $play;
    }



}