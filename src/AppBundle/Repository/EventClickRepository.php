<?php

namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;

class EventClickRepository extends EntityRepository
{
    /**
     * Executes the update statement
     * @param  \DateTime $date
     * @param  string    $country
     * @return integer           1 if successful (thanks to unique key), 0 otherwise
     */
    public function doInc($date, $country)
    {
        return $this->_em
            ->createQuery('
               UPDATE AppBundle:EventClickLog evl
               SET evl.hits = evl.hits + 1
               WHERE evl.date = :date AND evl.country = :country
           ')
            ->setParameter('date', $date)
            ->setParameter('country', $country)
            ->execute();
    }

    public function findAllFrom7DaysAgo($date)
    {
        $qb = $this->_em->createQueryBuilder();

        return $qb
            ->select('evl')
            ->from('AppBundle:EventClickLog', 'evl')
            ->where('evl.date = :date')
            ->setParameter('date', new \AppBundle\Type\DateX('@'.$date->format('U')))
            ->getQuery()
            ->execute()
            ;
    }

    public function sumPast7DaysForCountry($country, $date)
    {
        $qb = $this->_em->createQueryBuilder();

        return $qb
            ->select('SUM(evl.hits)')
            ->from('AppBundle:EventClickLog', 'evl')
            ->where('evl.country = :country')
            ->setParameter('country', $country)
            ->andWhere('evl.date > :date')
            ->setParameter('date',  new \AppBundle\Type\DateX('@'.$date->format('U')))
            ->getQuery()
            ->getSingleScalarResult()
            ;
    }
}