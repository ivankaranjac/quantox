<?php

namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;

class EventSummaryRepository extends EntityRepository
{
    public function doSumView($country, $hits)
    {
        return $this->_em
            ->createQuery('
               UPDATE AppBundle:EventSummary evl
               SET evl.view = evl.view + :hits
               WHERE evl.country = :country
           ')
            ->setParameter('hits', $hits)
            ->setParameter('country', $country)
            ->execute();
    }

    public function doSumClick($country, $hits)
    {
        return $this->_em
            ->createQuery('
               UPDATE AppBundle:EventSummary evl
               SET evl.click = evl.click + :hits
               WHERE evl.country = :country
           ')
            ->setParameter('hits', $hits)
            ->setParameter('country', $country)
            ->execute();
    }

    public function doSumPlay($country, $hits)
    {
        return $this->_em
            ->createQuery('
               UPDATE AppBundle:EventSummary evl
               SET evl.play = evl.play + :hits
               WHERE evl.country = :country
           ')
            ->setParameter('hits', $hits)
            ->setParameter('country', $country)
            ->execute();
    }

    public function findTop5CountriesView()
    {
        $qb = $this->_em->createQueryBuilder();

        return $qb
            ->select('es')
            ->from('AppBundle:EventSummary', 'es')
            ->orderBy('es.view', 'DESC')
            ->setMaxResults(5)
            ->getQuery()
            ->execute()
        ;
    }

    public function findTop5CountriesClick()
    {
        $qb = $this->_em->createQueryBuilder();

        return $qb
            ->select('es')
            ->from('AppBundle:EventSummary', 'es')
            ->orderBy('es.click', 'DESC')
            ->setMaxResults(5)
            ->getQuery()
            ->execute()
            ;
    }

    public function findTop5CountriesPlay()
    {
        $qb = $this->_em->createQueryBuilder();

        return $qb
            ->select('es')
            ->from('AppBundle:EventSummary', 'es')
            ->orderBy('es.play', 'DESC')
            ->setMaxResults(5)
            ->getQuery()
            ->execute()
            ;
    }
}